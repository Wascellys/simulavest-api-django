from rest_framework import routers
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from .api.viewsets import UserViewSet
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'users', UserViewSet, base_name='users')

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls)
]
