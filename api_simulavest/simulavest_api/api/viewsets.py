from user.models import Users, Type
from rest_framework.viewsets import ModelViewSet
from .serializers import UsersSerializer
from django.contrib.auth.models import User
from django.db import transaction
from django.shortcuts import render, get_object_or_404

class UserViewSet(ModelViewSet):
    queryset = Users.objects.all()
    serializer_class = UsersSerializer

    def get_queryset(self):
        return Users.objects.all()

    '''
    @transaction.atomic()
    def create(self,request, *args, **kwargs):
        
        usuario = Users()
        user = User()
        nome = request.data['name'].split(' ')
        primNome = ''
        segNome = ''


        for n in range(len(nome)):
            if (n < (len(nome) / 2.0)):
                primNome += nome[n] + ' '
            else:
                segNome += nome[n] + ' '

        usuario.name = request.data['name']
        usuario.alias = request.data['alias']
        tipo = Users.objects.filter(id=request.data['type'])
        usuario.type = tipo
        usuario.serie = request.data['serie']
        usuario.school = request.data['school']
        usuario.record = request.data['record']
        usuario.email = request.data['email']




        user.first_name = primNome
        user.last_name = segNome
        user.email = usuario.email

        print(usuario)
        print(user)
        #user.save()

        #usuario.user = user

        #usuario.save()
        '''