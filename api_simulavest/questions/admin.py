from django.contrib import admin
from questions.models import Item, Questions

admin.site.register(Questions)
admin.site.register(Item)
