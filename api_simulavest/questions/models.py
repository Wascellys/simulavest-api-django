from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Item(models.Model):
    description = models.CharField(max_length=100, null=False, blank=False)
    answer = models.BooleanField()
    

    def __str__(self):
        return self.description

class Questions(models.Model):
    description = models.CharField(max_length=100, null=False, blank=False)
    source = models.CharField(max_length=100, null=False, blank=False)
    period = models.CharField(max_length=100, null=False, blank=False)
    itens = models.ForeignKey(Item, on_delete=models.PROTECT)
    area = models.CharField(max_length=100, null=False, blank=False)
    specific = models.CharField(max_length=100, null=False, blank=False)
    subarea = models.CharField(max_length=100, null=False, blank=False)
    active = models.BooleanField
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    record = models.DateField
    level = models.IntegerField(default=0)

    def __str__(self):
        return self.description


