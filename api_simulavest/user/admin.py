from django.contrib import admin
from .models import Users, Type

# Register your models here.

admin.site.register(Type)
admin.site.register(Users)
