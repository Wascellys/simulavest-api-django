from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Type(models.Model):
    type = models.CharField(max_length=100, blank=False, null = False)

    def __str__(self):
        return self.type


class Users(models.Model):

    user = models.OneToOneField(User, on_delete=models.PROTECT)
    name = models.CharField(blank=False, max_length=100)
    
    alias = models.CharField(blank=False, max_length=100)
    type = models.ForeignKey(Type, on_delete=models.PROTECT, null=False, blank=False)
    email = models.CharField(max_length=100, blank=False, null=False)

    record = models.DateField(blank=True, null= True)
    
    school = models.CharField(max_length=50, blank=True, null=True)
    serie = models.CharField(max_length=50,blank=True, null=True)


    def __str__(self):
        return self.name


